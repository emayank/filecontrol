#include <stdio.h>
#include <string>

using namespace std;

#define LOG(...) fprintf(stderr, __VA_ARGS__)

class Socket
{
private:
	int _fd;

public:
	Socket();
	Socket(int fd);
	bool valid();
	int getFd();
	void close();

	void read(string& input);
	void send(const char* data, int len);
};
