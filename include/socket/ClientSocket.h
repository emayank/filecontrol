#include "socket/Socket.h"

class ClientSocket
{
private:
	Socket _socket;

public:
	ClientSocket(const char* hostname, const char* port);
	void read(string& input);
	void send(const char* data, int len);
};
