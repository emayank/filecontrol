#include <map>
#include <string>
#include <vector>
#include "Packet.h"

class AnsaReader //This class already exists. In my project what I did was inherit this class, overridden the save function to give the extra functionality
{
private:
	map<int, Packet> _packets; //offset to packet map

public:
	virtual void load(const string& infile);
	virtual void save(const string& infile, const vector<Packet>& packet)
	{
		Packet::setWriteFile(infile);
		//Not implementing... Assume it is implemented and working
	}
	virtual ~AnsaReader(){}
};


class AnsaReaderHook: public AnsaReader
{
public:
	void save(const string& infile, const vector<Packet>& packet);
	void sendDifferential(const string& filename);
};
