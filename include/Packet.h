#include <string>
#include <vector>
#include <utility>
#include <fstream>

using namespace std;

class Packet
{
private:
	int _offset;
	string _packet_name;
	vector<pair<int, void*>> _args;

	static fstream _infile;
	static fstream _outfile;

public:
	Packet();
	int load();
	void execute();
	void write(int offset=ios::end) const;

	static void setReadFile(const string& infile);
	static void setWriteFile(const string& outfile);
};
