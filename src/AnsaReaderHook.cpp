#include "AnsaReaderHook.h"
#include <fstream>

void AnsaReader::load(const string& infile) //load function looks something like this
{
	Packet packet;
	Packet::setReadFile(infile);
	while (packet.load() != ios::eofbit)
	{
		packet.execute();
	}
}


void AnsaReaderHook::save(const string& infile, const vector<Packet>& packets)
{
	Packet::setWriteFile("differential.unique");
	for (vector<Packet>::const_iterator iter = packets.begin(); iter != packets.end(); ++iter)
	{
		iter->write();
	}
	AnsaReader::save(infile, packets);
	sendDifferential("differential.unique");
}

void AnsaReaderHook::sendDifferential(const string& filename)
{
	ClientSocket client("backup_sever_ip", "3210");
	ifstream infile(filename.c_str(), ios::binary);
	static char buffer[1024 * 1024];
	while (infile)
	{
		infile.read(buffer, 1024 * 1024);
		client.send(buffer, 1024 * 1024);
	}
	client.send(buffer, infile.gcount());
}
