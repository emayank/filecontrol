#include "socket/Socket.h"
#include <sys/socket.h>
#include <sys/ioctl.h>

Socket::Socket():_fd(-1)
{
}

Socket::Socket(int fd):_fd(fd)
{
}

int Socket::getFd()
{
	return _fd;
}

void Socket::close()
{
	::close(_fd);
}

bool Socket::valid()
{
	return _fd > 0;
}

void Socket::read(string& input) //This is wrong... This cannot be a string in case of Ansa Project where I am sending binary data over the network... Why?
{
	char temp[1] = {0};
	::recv(_fd, temp, 1, 0); //read the first character of the data. recv will wait till no data reaches this socket

	int len = 0;
	ioctl(_fd, FIONREAD, &len); //ioctl to find out the length of available data on socket
	if (len > 0) {
		char* buffer = new char[len];
		buffer[0] = temp[0];
		::recv(_fd, buffer + 1, len - 1, 0);
		input = buffer;
	}
}

//void Socket::send(const string& data) //Similarly this is also wrong
void Socket::send(const char* data, int len)
{
	::send(_fd, data, len, 0);
}
