#include <sys/socket.h>
#include <netdb.h>
#include "socket/ClientSocket.h"

ClientSocket::ClientSocket(const char* hostname, const char* port)
{
	addrinfo hints;
	addrinfo* res;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;  // Not specifying which of IPv4 or v6 to be used
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;     // This system's IP

	int fd = -1;
	int result = getaddrinfo(hostname, port, &hints, &res);
	if (result != 0)
	{
		LOG("getaddrinfo: %s\n", gai_strerror(result));
	}
	else
	{
		for (; res != NULL && fd == -1; res = res->ai_addr)
		{
			fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		}
		if (fd == -1)
		{
			LOG("Error while creating socket");
		}
		else
		{
			result = connect(fd, res->ai_addr, res->ai_addrlen);
			if (result == -1)
			{
				LOG("Could not bind connect");
				close(fd);
			}
			else
			{
				_socket = Socket(fd);
			}
		}
	}
}

void ClientSocket::read(string& input)
{
	_socket.read(input);
}

void ClientSocket::send(const char* data, int len)
{
	_socket.send(data, len);
}
