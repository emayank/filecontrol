#include "Packet.h"
#include <dlfcn.h>

fstream Packet::_infile; //See here... fstream is used!
fstream Packet::_outfile; //See here... fstream is used!

Packet::Packet(): _offset(-1)
{
}

int Packet::load()
{
	_infile.read(reinterpret_cast<char*>(&_offset), sizeof(int));
	int packet_name_size;
	_infile.read(reinterpret_cast<char*>(&packet_name_size), sizeof(int));
	char* packet_name = new char[packet_name_size];
	_infile.read(packet_name, packet_name_size);
	_packet_name = packet_name;
	delete[] packet_name;
	int args_count;
	_infile.read(reinterpret_cast<char*>(&args_count), sizeof(int));
	for (int count = 0; count < args_count; ++count)
	{
		int arg_size;
		_infile.read(reinterpret_cast<char*>(&arg_size), sizeof(int));
		char* arg = new char[arg_size];
		_infile.read(arg, arg_size);
		_args.push_back(pair<int, void*>(arg_size, arg));
	}
	return _infile.tellg();
}

void Packet::execute()
{
	void* handle = dlopen("ansacore.so", RTLD_LAZY); //open a library/shared object/so file to find out the function pointer/address of the function with name _packet_name
	void* fp = dlsym(handle, _packet_name.c_str()); //get the function pointer of the function present in ansocore.so
	fp(_args); //call that function using these arguments
}

void Packet::write(int offset) const
{
	if (_outfile.is_open())
	{
		_outfile.seekp(offset);
		_offset = offset;

		_outfile.write(reinterpret_cast<char*>(_offset), sizeof(int));
		_outfile.write(reinterpret_cast<char*>(_packet_name.size() + 1), sizeof(int));
		_outfile.write(_packet_name.c_str(), _packet_name.size() + 1);
		_outfile.write(reinterpret_cast<char*>(_args.size()), sizeof(int));
		for (vector<pair<int, void*>>::iterator iter = _args.begin(); iter != _args.end(); ++iter)
		{
			_outfile << iter->first << iter->second;
			_outfile.write(reinterpret_cast<char*>(iter->first), sizeof(int));
			_outfile.write(reinterpret_cast<char*>(iter->second), iter->first);
		}
	}
}

static void Packet::setWriteFile(const string& outfile)
{
	if (_outfile.is_open())
	{
		_outfile.close();
	}
	_outfile.open(outfile.c_str(), ios::binary | ios::in | ios::out);
}

static void Packet::setReadFile(const string& infile)
{
	if (_infile.is_open())
	{
		_infile.close();
	}
	_infile.open(infile.c_str(), ios::binary | ios::in);
}
